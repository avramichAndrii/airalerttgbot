﻿using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using System.IO;
using System.Text.Json.Nodes;

namespace AirAlertGifBot
{
    internal class Program
    {
        private static ITelegramBotClient _botClient;

        private const string botKey = "6744815456:AAFpYx0eEcNYoTvpuU95lShJUbbwPDwn-V8";
        private const string AlertAPIURL = "http://ubilling.net.ua/aerialalerts/";
        private const int TimerConst = 120000;

        private static ReceiverOptions _receiverOptions;

        static async Task Main()
        {
            _botClient = new TelegramBotClient(botKey); 
            _receiverOptions = new ReceiverOptions 
            {
                AllowedUpdates = new[]
                {
                UpdateType.Message,
            },

                ThrowPendingUpdates = true,
            };

            using var cts = new CancellationTokenSource();

            _botClient.StartReceiving(UpdateHandler, ErrorHandler, _receiverOptions, cts.Token); 

            var me = await _botClient.GetMeAsync(); 
            Console.WriteLine($"{me.FirstName} started!");

            await Task.Delay(-1);
        }

        private static async Task UpdateHandler(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            try
            {
                switch (update.Type)
                {
                    
                    case UpdateType.Message:
                        {
                            Console.WriteLine($"Update received");
                            while (true)
                            {
                                Console.WriteLine($"Inside loop 2 min interval pool");
                                Thread.Sleep(TimerConst);
                                var result = GetAlertResult();
                                Console.WriteLine($"Result is {result}");
                                if (result)
                                {
                                    Console.WriteLine($"Alert is active");
                                    InputFile file;
                                    var message = update.Message;

                                    var user = message.From;

                                    var pathToGif = GetRandomPath();
                                    Console.WriteLine($"gif path is {pathToGif}");

                                    using (var stream = new FileStream(pathToGif, FileMode.Open))
                                    {
                                        file = InputFile.FromStream(stream, "alert");

                                        Console.WriteLine($"{user.FirstName} ({user.Id}) write a message: {message.Text}");

                                        var chat = message.Chat;
                                        await botClient.SendVideoAsync(chat.Id, file);
                                        Console.WriteLine($"Message is sent");
                                    }
                                }
                            }

                            return;
                        }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private static Task ErrorHandler(ITelegramBotClient botClient, Exception error, CancellationToken cancellationToken)
        {
            var ErrorMessage = error switch
            {
                ApiRequestException apiRequestException
                    => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => error.ToString()
            };

            Console.WriteLine(ErrorMessage);
            return Task.CompletedTask;
        }
        private static string GetRandomPath()
        {
            var gifsPath = Directory.GetFiles("../../../AlertGifs");
            Random random = new Random();
            int start2 = random.Next(0, gifsPath.Length);
            return gifsPath[start2];
        }

        private static bool GetAlertResult()
        {
            var content = new HttpClient().GetAsync(AlertAPIURL).Result.Content.ReadAsStringAsync().Result;
            var json = JsonNode.Parse(content);
            var result = json["states"]["Київська область"]["alertnow"];
            return Convert.ToBoolean(result);
        }
    }
}